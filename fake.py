import cv2
import numpy as np
import os
import time

# Generates images from the given encoded image
###################################################
# Inputs:
#########
#   im      => Encoded Cv2 image
#   Path    => Path of where the image will be saved

##################################3
# Outputs:
##########
#   Nothing is returned, But 2 Images are saved in the given path


def GenerateImages(im,Path,image_name):
    img = cv2.imdecode(im, cv2.IMREAD_COLOR)
    for i in range(3):
        #PathToSaveImage= os.path.join(Path,str(i)+str(time.time())+".png")
        PathToSaveImage= os.path.join(Path,image_name+"_0"+str(i)+".png")
        EditedImage= np.zeros(img.shape)
        EditedImage[:,:,i]= img[:,:,i]
        cv2.imwrite(PathToSaveImage,EditedImage)


def GenerateImagesDelayed(im,Path,image_name):
    time.sleep(10)
    GenerateImages(im,Path,image_name)
    time.sleep(10)


def TestFunctions(PathOfImageToRead,PathOfImageToSave):
    image= cv2.imread(PathOfImageToRead)
    image_encoded= cv2.imencode('.jpg', image)
    image_encoded= image_encoded[1]
    GenerateImages(image_encoded,PathOfImageToSave)
    GenerateImagesDelayed(image_encoded,PathOfImageToSave)

#TestFunctions("SaadSand.jpg","./SavedData/")
    